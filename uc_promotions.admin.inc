<?php

/**
 * @file
 * Promotion form.
 */

/**
 * Create the promotions for products.
 */
function uc_promotions_create_form($form, &$form_state, $promo = NULL) {
  if ($promo) {
    $form_state['values'] = (array) $promo;
  }
  $form = array();
  $form['buy_promotion_on'] = array(
    '#title' => t('Category/Product'),
    '#type' => 'select',
    '#options' => array(
      'product' => t('Product'),
      'category' => t('Category'),
    ),
    '#default' => empty($form_state['values']['buy_promotion_on']) ? '' :
        $form_state['values']['buy_promotion_on'],
    '#ajax' => array(
      'callback' => 'uc_promotions_buy_ajax',
      'wrapper' => 'buy-promotion-wrapper',
    ),
    '#required' => TRUE,
  );

  $form['promotion_buy'] = array(
    '#type' => 'field',
    '#title' => t('Promotion information'),
    '#prefix' => '<div id="buy-promotion-wrapper">',
    '#suffix' => '</div>',
  );

  if (!empty($form_state['values']['buy_promotion_on'])) {
    if ($form_state['values']['buy_promotion_on'] == 'product') {
      $form['promotion_buy']['buy_sku'] = array(
        '#type' => 'textfield',
        '#title' => t('Product SKU'),
        '#description' => t('Enter the SKU/Title of the product to add to the promotion.'),
        '#autocomplete_path' => 'uc_product/autocomplete',
        '#default_value' => empty($form_state['values']['buy_sku']) ? '' :
            $form_state['values']['buy_sku'],
        '#size' => 60,
        '#maxlength' => 255,
        '#required' => TRUE,
      );
    }
    else if ($form_state['values']['buy_promotion_on'] == 'category') {

      if ($tree = taxonomy_get_tree(2)) {
        $options = array();
        foreach ($tree as $item) {
          $options[$item->tid] = str_repeat('-', $item->depth) . $item->name;
        }
        $form['promotion_buy']['buy_category'] = array(
          '#type' => 'select',
          '#title' => t('Catalog'),
          '#options' => $options,
          '#default' => empty($form_state['values']['buy_category']) ? '' :
              $form_state['values']['buy_category'],
          '#required' => TRUE,
        );
      }
    }
  }

  $form['promo_type'] = array(
    '#title' => t('Select Promotion Type'),
    '#type' => 'select',
    '#options' => array(
      'bxgx' => t('Buy X Get X'),
      'bxgy' => t('Buy X Get Y'),
      'pp' => t('Power Pricing'),
      'bvd' => t('Bulk Value Discount'),
    ),
    '#default' => empty($form_state['values']['promo_type']) ? '' :
        $form_state['values']['promo_type'],
    '#ajax' => array(
      'callback' => 'uc_promotions_ajax',
      'wrapper' => 'promotion-wrapper',
    ),
    '#required' => TRUE,
  );

  $form['promotion'] = array(
    '#type' => 'field',
    '#title' => t('Promotion information'),
    '#prefix' => '<div id="promotion-wrapper">',
    '#suffix' => '</div>',
  );

  if (!empty($form_state['values']['promo_type'])) {
    if ($form_state['values']['promo_type'] != 'bvd') {
      $form['promotion']['buy_qty'] = array(
        '#type' => 'textfield',
        '#default_value' => empty($form_state['values']['buy_qty']) ? '' : $form_state['values']['buy_qty'],
        '#maxlength' => 30,
        '#title' => t('Buy Quantity'),
        '#description' => t('Enter the buy qty of product.'),
        '#required' => TRUE,
      );
    }
  }

  $form['promotion']['discount_type'] = array(
    '#title' => t('Select Discount Type'),
    '#type' => 'select',
    '#options' => array(
      'free' => t('FREE'),
      'percent_off' => t('PERCENT OFF'),
      'fix_amount' => t('FIX AMOUNT'),
      'flat_off' => t('FLAT OFF'),
    ),
    '#default' => empty($form_state['values']['discount_type']) ? '' :
        $form_state['values']['discount_type'],
    '#ajax' => array(
      'callback' => 'uc_promotions_ajax',
      'wrapper' => 'promotion-wrapper',
    ),
    '#required' => TRUE,
  );

  if (!empty($form_state['values']['discount_type'])) {
    if ($form_state['values']['discount_type'] != 'free') {
      $form['promotion']['amount'] = array(
        '#type' => 'textfield',
        '#default_value' => empty($form_state['values']['amount']) ? '' : $form_state['values']['amount'],
        '#maxlength' => 60,
        '#title' => t('Amount'),
        '#description' => t('Enter the discount/fix amount on offer product.'),
        '#required' => TRUE,
      );
      if ($form_state['values']['discount_type'] == 'percent_off') {
        $form['promotion']['amount']['#title'] = t('Percentage');
        $form['promotion']['amount']['#description'] = t('Enter the discount percentage on offer product.');
      }
    }

    if ($form_state['values']['promo_type'] != 'bxgx') {
      $form['promotion']['offer_promotion_on'] = array(
        '#title' => t('Category/Product'),
        '#type' => 'select',
        '#options' => array(
          'product' => t('Product'),
          'category' => t('Category'),
        ),
        '#default' => empty($form_state['values']['offer_promotion_on']) ? '' :
        $form_state['values']['offer_promotion_on'],
        '#ajax' => array(
          'callback' => 'uc_promotions_offer_ajax',
          'wrapper' => 'offer-promotion-wrapper',
        ),
        '#required' => TRUE,
      );
      $form['promotion']['promotion_offer'] = array(
        '#type' => 'field',
        '#title' => t('Promotion information'),
        '#prefix' => '<div id="offer-promotion-wrapper"><div id="offer-promotion-wrapper"><div></div>',
        '#suffix' => '</div>',
      );
      if (!empty($form_state['values']['offer_promotion_on'])) {
        if ($form_state['values']['offer_promotion_on'] == 'product') {
          $form['promotion']['promotion_offer']['offer_sku'] = array(
            '#type' => 'textfield',
            '#title' => t('Offer Product SKU'),
            '#description' => t('Enter the SKU/Title of the offer product to add to the promotion.'),
            '#default_value' => empty($form_state['values']['offer_sku']) ? '' : $form_state['values']['offer_sku'],
            '#autocomplete_path' => 'uc_product/autocomplete',
            '#size' => 60,
            '#maxlength' => 255,
            '#required' => TRUE,
          );
        } else if ($form_state['values']['offer_promotion_on'] == 'category') {
          if ($tree = taxonomy_get_tree(2)) {
            $options = array();
            foreach ($tree as $item) {
              $options[$item->tid] = str_repeat('-', $item->depth) . $item->name;
            }
            $form['promotion']['promotion_offer']['offer_category'] = array(
              '#type' => 'select',
              '#title' => t('Catalog'),
              '#options' => $options,
              '#default' => empty($form_state['values']['offer_category']) ? '' :
              $form_state['values']['offer_category'],
              '#required' => TRUE,
            );
          }
        }
      }
    }

    $form['promotion']['offer_qty'] = array(
      '#type' => 'textfield',
      '#default_value' => empty($form_state['values']['offer_qty']) ? '' : $form_state['values']['offer_qty'],
      '#maxlength' => 60,
      '#title' => t('Offer Quantity'),
      '#description' => t('Enter the offer qty of product.'),
      '#required' => TRUE,
    );
  }

  $form['valid_from'] = array(
    '#type' => 'date_popup',
    '#title' => t('Valid From'),
    '#date_type' => DATE_UNIX,
    '#date_label_position' => '',
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '0:+1',
    // '#date_timezone' => date_default_timezone(),
    '#default_value' => empty($form_state['values']['valid_from']) ? '' : $form_state['values']['valid_from'],
    '#required' => TRUE,
  );

  $form['valid_upto'] = array(
    '#type' => 'date_popup',
    '#title' => t('Valid Upto'),
    '#date_type' => DATE_UNIX,
    '#date_label_position' => '',
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '0:+3',
    // '#date_timezone' => date_default_timezone(),
    '#default_value' => empty($form_state['values']['valid_from']) ? '' : $form_state['values']['valid_from'],
    '#required' => TRUE,
  );

  $form['offer_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Promotion Description'),
    '#description' => t('Enter the description of promotion.'),
    '#default_value' => empty($form_state['values']['offer_description']) ? '' :
        $form_state['values']['offer_description'],
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create promotion'),
  );
  return $form;
}

/**
 * Form submission handler for uc_promotion_create_form().
 *
 * @see uc_promotion_create_form()
 */
function uc_promotions_create_form_submit($form, &$form_state) {
//   print_r($form_state);  
//   exit; 
  db_insert('uc_promotions')
      ->fields(array(
        'promo_type' => $form_state['values']['promo_type'],
        'buy_sku' => empty($form_state['values']['buy_sku']) ? NULL :
        $form_state['values']['buy_sku'],
        'buy_category' => empty($form_state['values']['buy_category']) ? NULL :
        $form_state['values']['buy_category'],
        'discount_type' => $form_state['values']['discount_type'],
        'amount' => empty($form_state['values']['amount']) ? 0 :
        $form_state['values']['amount'],
        'buy_qty' => $form_state['values']['buy_qty'],
        'offer_sku' => empty($form_state['values']['offer_sku']) ? NULL :
        $form_state['values']['offer_sku'],
        'offer_category' => empty($form_state['values']['offer_category']) ? NULL :
        $form_state['values']['offer_category'],
        'offer_qty' => $form_state['values']['offer_qty'],
        'valid_from' => $form_state['values']['valid_from'],
        'valid_upto' => $form_state['values']['valid_upto'],
        'created' => time(),
        'changed' => time(),
        'offer_description' => $form_state['values']['offer_description'],
      ))
      ->execute();
  drupal_set_message(t('Promotion saved.'));
}

/**
 * Ajax callback to change promotion fields.
 */
function uc_promotions_ajax($form, &$form_state) {
  return $form['promotion'];
}

/**
 * Ajax callback to change promotion fields.
 */
function uc_promotions_buy_ajax($form, &$form_state) {
  return $form['promotion_buy'];
}

/**
 * Ajax callback to change promotion fields.
 */
function uc_promotions_offer_ajax($form, &$form_state) {
  return $form['promotion']['promotion_offer'];
}

/**
 * Page callback: Form constructor for the promotions administration form.
 *
 * @see node_admin_nodes()
 * @see node_admin_nodes_submit()
 * @see node_admin_nodes_validate()
 * @see node_filter_form()
 * @see node_filter_form_submit()
 * @see node_menu()
 * @see node_multiple_delete_confirm()
 * @see node_multiple_delete_confirm_submit()
 * @ingroup forms
 */
function admin_promotions($form, $form_state) {
  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    return uc_promotions_multiple_delete_confirm($form, $form_state, array_filter($form_state['values']['promotions']));
  }
  $form['filter'] = uc_promotions_filter_form();
  $form['#submit'][] = 'uc_promotions_filter_form_submit';
  $form['admin'] = uc_promotions_list();

  return $form;
}

/**
 * Form builder: Builds the node administration overview.
 *
 * @see node_admin_nodes_submit()
 * @see node_admin_nodes_validate()
 * @see node_filter_form()
 * @see node_filter_form_submit()
 * @see node_multiple_delete_confirm()
 * @see node_multiple_delete_confirm_submit()
 *
 * @ingroup forms
 */
function uc_promotions_list() {
  $promo_access = uc_promotions_access('delete');
  // Build the 'Update options' form.
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
  );
  $options = array();

  $options['select'] = t('-Select-');
  $options['delete'] = t('Delete the selected promotions');
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => 'approve',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#validate' => array('uc_promotions_list_validate'),
    '#submit' => array('uc_promotions_list_submit'),
  );

  // Build the sortable table header.
  $header = array(
    'promo_type' => array('data' => t('Promotion Type'), 'field' => 'p.promo_type'),
    'buy_sku' => array('data' => t('Product Sku'), 'field' => 'p.buy_sku'),
    'discount_type' => array('data' => t('Discount Type'), 'field' => 'p.discount_type'),
    'amount' => array('data' => t('Amount'), 'field' => 'p.amount'),
    'buy_qty' => array('data' => t('Buy Qty'), 'field' => 'p.buy_qty', 'sort' => 'desc')
  );
  $header['operations'] = array('data' => t('Operations'));

  $query = db_select('uc_promotions', 'p')->extend('PagerDefault')->extend('TableSort');
  uc_promotions_build_filter_query($query);

  $promos = $query
      ->fields('p')
      ->limit(50)
      ->orderByHeader($header)
      ->execute()
      ->fetchAll();

  // Prepare the list of promotions.
  $destination = drupal_get_destination();
  $options = array();
  foreach ($promos as $promo) {
    $options[$promo->promo_id] = array(
      'promo_type' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $promo->promo_type,
          '#href' => 'node/' . $promo->promo_id,
          '#suffix' => ' ' . theme('mark', array('type' => node_mark($promo->promo_id, $promo->buy_qty))),
        ),
      ),
      'buy_sku' => $promo->buy_sku,
      'discount_type' => $promo->discount_type,
      'amount' => $promo->amount,
      'buy_qty' => $promo->buy_qty,
        //'buy_qty' => format_date($node->changed, 'short'),
    );
    // Build a list of all the accessible operations for the current promotion.
    $operations = array();
    $operations['edit'] = array(
      'title' => t('edit'),
      'href' => 'admin/promotion/' . $promo->promo_id . '/edit',
      'query' => $destination,
    );
    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => 'admin/promotion/' . $promo->promo_id . '/delete',
      'query' => $destination,
    );
    $options[$promo->promo_id]['operations'] = array();
    if (count($operations) > 1) {
      // Render an unordered list of operations links.
      $options[$promo->promo_id]['operations'] = array(
        'data' => array(
          '#theme' => 'links__node_operations',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );
    }
    elseif (!empty($operations)) {
      // Render the first and only operation as a link.
      $link = reset($operations);
      $options[$promo->promo_id]['operations'] = array(
        'data' => array(
          '#type' => 'link',
          '#title' => $link['title'],
          '#href' => $link['href'],
          '#options' => array('query' => $link['query']),
        ),
      );
    }
  }
  if ($promo_access) {
    $form['promotions'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
    );
  }
  // Otherwise, use a simple table.
  else {
    $form['promotions'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $options,
      '#empty' => t('No content available.'),
    );
  }

  $form['pager'] = array('#markup' => theme('pager'));
  return $form;
}

/**
 * Validate uc_promotions_list form submissions.
 *
 * Checks whether any promotions have been selected to perform the chosen 'Update
 * option' on.
 *
 * @see uc_promotions_list()
 * @see uc_promotions_list_submit()
 * @see node_filter_form()
 * @see node_filter_form_submit()
 * @see node_multiple_delete_confirm()
 * @see node_multiple_delete_confirm_submit()
 */
function uc_promotions_list_validate($form, &$form_state) {
  // print_r($form_state);exit;
  // Error if there are no items to select.
  if (!is_array($form_state['values']['promotions']) || !count(array_filter($form_state['values']['promotions']))) {
    form_set_error('', t('No items selected.'));
  }
}

/**
 * Process node_admin_nodes form submissions.
 *
 * Executes the chosen 'Update option' on the selected nodes.
 *
 * @see node_admin_nodes()
 * @see node_admin_nodes_validate()
 * @see node_filter_form()
 * @see node_filter_form_submit()
 * @see node_multiple_delete_confirm()
 * @see node_multiple_delete_confirm_submit()
 */
function uc_promotions_list_submit($form, &$form_state) {
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked nodes
  $promos = array_filter($form_state['values']['promotions']);
  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($nodes), $operation['callback arguments']);
    }
    else {
      $args = array($nodes);
    }

    call_user_func_array($function, $args);

    cache_clear_all();
  }
  else {
    // We need to rebuild the form to go to a second step. For example, to
    // show the confirmation form for the deletion of nodes.
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Returns the promotions administration filters form array to node_admin_content().
 *
 * @see uc_promotions_list()
 * @see uc_promotions_list_submit()
 * @see uc_promotions_list_validate()
 * @see uc_promotions_filter_form_submit()
 * @see node_multiple_delete_confirm()
 * @see node_multiple_delete_confirm_submit()
 *
 * @ingroup forms
 */
function uc_promotions_filter_form() {
  $session = isset($_SESSION['promo_overview_filter']) ? $_SESSION['promo_overview_filter'] : array();
  $filters = uc_promotions_filters();

  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only items where'),
  );

  foreach ($session as $filter) {
    list($type, $value) = $filter;
    $value = $filters[$type]['options'][$value];

    $t_args = array('%property' => $filters[$type]['title'], '%value' => $value);
    if ($i++) {
      $form['filters']['current'][] = array('#markup' => t(' and where %property is %value', $t_args));
    }
    else {
      $form['filters']['current'][] = array('#markup' => t('where %property is %value', $t_args));
    }
//    if (in_array($type, array('promo_type', 'discount_type'))) {
//      // Remove the option if it is already being filtered on.
//      unset($filters[$type]);
//    }
  }
  foreach ($filters as $key => $filter) {
    $form['filters']['promo_type']['filters'][$key] = array(
      '#type' => 'select',
      '#options' => $filter['options'],
      '#title' => $filter['title'],
      '#default_value' => '[any]',
    );
  }

  $form['filters']['promo_type']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['promo_type']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => count($session) ? t('Refine') : t('Filter'),
  );
  if (count($session)) {
    $form['filters']['promo_type']['actions']['undo'] = array('#type' => 'submit', '#value' => t('Undo'));
    $form['filters']['promo_type']['actions']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));
  }

  return $form;
}

function uc_promotions_filter_form_submit($form, $form_state) {
  $filters = uc_promotions_filters();
  switch ($form_state['values']['op']) {
    case t('Filter'):
    case t('Refine'):
      // Apply every filter that has a choice selected other than 'any'.
      foreach ($filters as $filter => $options) {
        if (isset($form_state['values'][$filter]) && $form_state['values'][$filter] != '[any]') {
          // Flatten the options array to accommodate hierarchical/nested options.
          $flat_options = form_options_flatten($filters[$filter]['options']);
          // Only accept valid selections offered on the dropdown, block bad input.
          if (isset($flat_options[$form_state['values'][$filter]])) {
            $_SESSION['promo_overview_filter'][] = array($filter, $form_state['values'][$filter]);
          }
        }
      }
      break;
    case t('Undo'):
      array_pop($_SESSION['promo_overview_filter']);
      break;
    case t('Reset'):
      $_SESSION['promo_overview_filter'] = array();
      break;
  }
}

/**
 * List promotion administration filters that can be applied.
 *
 * @return
 *   An associative array of filters.
 */
function uc_promotions_filters() {
  // Regular filters
  $filters['promo_type'] = array(
    'title' => t('Promotion Type'),
    'options' => array(
      '[any]' => t('any'),
      'bxgx' => t('Buy X Get X'),
      'bxgy' => t('Buy X Get Y'),
      'pp' => t('Power Pricing'),
      'bvd' => t('Bulk Value Discount'),
    ),
  );

  $filters['discount_type'] = array(
    'title' => t('Discount Type'),
    'options' => array(
      '[any]' => t('any'),
      'free' => t('FREE'),
      'percent_off' => t('PERCENT OFF'),
      'fix_amount' => t('FIX AMOUNT'),
      'flat_off' => t('FLAT OFF'),
    ),
  );

  return $filters;
}

/**
 * Applies filters for node administration filters based on session.
 *
 * @param $query
 *   A SelectQuery to which the filters should be applied.
 */
function uc_promotions_build_filter_query(SelectQueryInterface $query) {
  // Build query\
  $filter_data = isset($_SESSION['promo_overview_filter']) ? $_SESSION['promo_overview_filter'] : array();
  foreach ($filter_data as $index => $filter) {
    list($key, $value) = $filter;
    $query->condition('p.' . $key, $value);
  }
}
